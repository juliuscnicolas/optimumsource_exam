namespace OptimumSource_Exam.Repositories.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCommitdb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProbOne_Orders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Product_Id = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        AmountDue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CurrentProductQuantity = c.Int(nullable: false),
                        DatePurchased = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProbOne_Products",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProductCode = c.String(),
                        ProductDescription = c.String(),
                        Quantity = c.Int(nullable: false),
                        BulkOrder = c.Int(nullable: false),
                        MinimumOrder = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CriticalLevel = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProbThree_GameLogs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Gamer_Id = c.Int(nullable: false),
                        Start = c.DateTime(nullable: false),
                        End = c.DateTime(nullable: false),
                        EarnedPoints = c.Int(nullable: false),
                        CurrentPoints = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProbThree_Gamers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        GamerCode = c.String(),
                        Name = c.String(),
                        RegistrationDate = c.DateTime(nullable: false),
                        BirthDate = c.DateTime(nullable: false),
                        Points = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProbTwo_Orders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Product_Id = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        IsPatron = c.Boolean(nullable: false),
                        AmountDue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DatePurchased = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProbTwo_Products",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProductCode = c.String(),
                        ProductDescription = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDiscounted = c.Boolean(nullable: false),
                        Discount = c.Int(nullable: false),
                        DateAdded = c.DateTime(nullable: false),
                        CriticalLevel = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ProbTwo_Products");
            DropTable("dbo.ProbTwo_Orders");
            DropTable("dbo.ProbThree_Gamers");
            DropTable("dbo.ProbThree_GameLogs");
            DropTable("dbo.ProbOne_Products");
            DropTable("dbo.ProbOne_Orders");
        }
    }
}
