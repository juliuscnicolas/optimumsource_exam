﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using OptimumSource_Exam.Models;

namespace OptimumSource_Exam.Repositories.UnitOfWork
{
    public class OptimumSource_ExamDbContext : DbContext
    {
        public OptimumSource_ExamDbContext()
            : base("OptimumSource_Exam") { 
        
        }

        public DbSet<ProbOne_Products> ProbOne_Products { get; set; }
        public DbSet<ProbOne_Orders> ProbOne_Orders { get; set; }
        public DbSet<ProbTwo_Products> ProbTwo_Products { get; set; }
        public DbSet<ProbTwo_Orders> ProbTwo_Orders { get; set; }
        public DbSet<ProbThree_GameLogs> ProbThree_GameLogs { get; set; }
        public DbSet<ProbThree_Gamers> ProbThree_Gamers { get; set; }



    }
}
