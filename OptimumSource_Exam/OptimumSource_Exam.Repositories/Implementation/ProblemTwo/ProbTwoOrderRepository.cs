﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OptimumSource_Exam.Models;
using OptimumSource_Exam.Repositories.UnitOfWork;

namespace OptimumSource_Exam.Repositories.Implementation.ProblemTwo
{
    public class ProbTwoOrderRepository
    {

        public List<ProbTwo_Orders> GetAllOrders()
        {
            try
            {
                OptimumSource_ExamDbContext db = new OptimumSource_ExamDbContext();
                return db.ProbTwo_Orders.ToList();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool CreateOrders(ProbTwo_Orders orders)
        {
            try
            {
                OptimumSource_ExamDbContext db = new OptimumSource_ExamDbContext();
                db.ProbTwo_Orders.Add(orders);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool UpdateOrder(ProbTwo_Orders Order)
        {
            try
            {
                return true;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool DeleteOrder(ProbTwo_Orders Order)
        {
            try
            {
                return true;
            }
            catch (Exception)
            {

                throw;
            }
        }


        

    }
}
