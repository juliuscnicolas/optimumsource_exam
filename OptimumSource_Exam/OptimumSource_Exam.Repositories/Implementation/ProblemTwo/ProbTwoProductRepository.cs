﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OptimumSource_Exam.Models;
using OptimumSource_Exam.Repositories.UnitOfWork;

namespace OptimumSource_Exam.Repositories.Implementation.ProblemTwo
{
    public class ProbTwoProductRepository
    {
        public List<ProbTwo_Products> GetAllProducts()
        {
            try
            {
                OptimumSource_ExamDbContext db = new OptimumSource_ExamDbContext();
                return db.ProbTwo_Products.ToList();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool CreateProduct(ProbTwo_Products product)
        {
            try
            {
                OptimumSource_ExamDbContext db = new OptimumSource_ExamDbContext();
                db.ProbTwo_Products.Add(product);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public bool UpdateProduct(ProbTwo_Products product)
        {
            try
            {
                return true;
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public bool DeleteProduct(ProbTwo_Products product)
        {
            try
            {
                OptimumSource_ExamDbContext db = new OptimumSource_ExamDbContext();
                ProbTwo_Products prod = new ProbTwo_Products();
                prod = db.ProbTwo_Products.Where(prd => prd.Id == product.Id).FirstOrDefault<ProbTwo_Products>();

                using (var newContext = new OptimumSource_ExamDbContext())
                {
                    newContext.Entry(prod).State = System.Data.Entity.EntityState.Deleted;
                    newContext.SaveChanges();
                }  

                return true;
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        
    }
}
