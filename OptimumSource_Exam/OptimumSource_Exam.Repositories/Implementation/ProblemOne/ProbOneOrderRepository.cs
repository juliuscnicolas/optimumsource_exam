﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OptimumSource_Exam.Models;
using OptimumSource_Exam.Repositories.UnitOfWork;
using OptimumSource_Exam.Models.ViewModel.ProblemOne;
using System.Data.Entity.Core.Objects;

namespace OptimumSource_Exam.Repositories.Implementation.ProblemOne
{
    public class ProbOneOrderRepository
    {

        public List<ProductOrder> GetAllOrders()
        {
            try
            {
                OptimumSource_ExamDbContext db = new OptimumSource_ExamDbContext();
                
                var result = from order in db.ProbOne_Orders
		                     join product in db.ProbOne_Products on order.Product_Id equals product.Id
		                     select new ProductOrder { OrderId = order.Id, ProductCode = product.ProductCode, Quantity = order.Quantity, AmountDue = order.AmountDue, DatePurchased = order.DatePurchased, QuantityBecomes = order.CurrentProductQuantity.ToString() };

                return result.ToList<ProductOrder>();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool CreateOrders(ProbOne_Orders orders)
        {
            try
            {
                OptimumSource_ExamDbContext db = new OptimumSource_ExamDbContext();
                db.ProbOne_Orders.Add(orders);
                db.SaveChanges();

                ProbOne_Products product = new ProbOne_Products();
                product = db.ProbOne_Products.Where(prod => prod.Id == orders.Product_Id).FirstOrDefault();
                product.Quantity = Convert.ToInt32(orders.CurrentProductQuantity);
                db.Entry(product).State = System.Data.Entity.EntityState.Modified;

                db.SaveChanges();

                return true;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool UpdateOrder(ProbOne_Orders Order)
        {
            try
            {
                return true;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool DeleteOrder(ProbOne_Orders Order)
        {
            try
            {
                return true;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
