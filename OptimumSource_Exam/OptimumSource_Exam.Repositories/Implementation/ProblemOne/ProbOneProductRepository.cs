﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OptimumSource_Exam.Models;
using OptimumSource_Exam.Repositories.UnitOfWork;

namespace OptimumSource_Exam.Repositories.Implementation.ProblemOne
{
    public class ProbOneProductRepository
    {
        public List<ProbOne_Products> GetAllProducts()
        {
            try
            {
                OptimumSource_ExamDbContext db = new OptimumSource_ExamDbContext();
                return db.ProbOne_Products.ToList();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool CreateProduct(ProbOne_Products product)
        {
            try
            {
                OptimumSource_ExamDbContext db = new OptimumSource_ExamDbContext();
                db.ProbOne_Products.Add(product);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public bool UpdateProduct(ProbOne_Products product)
        {
            try
            {
                OptimumSource_ExamDbContext db = new OptimumSource_ExamDbContext();
                //ProbOne_Products prod = new ProbOne_Products();
                var prod =  db.ProbOne_Products.Where(prd => prd.Id == product.Id).FirstOrDefault();

                prod.Quantity = product.Quantity;
                prod.BulkOrder = product.BulkOrder;
                prod.MinimumOrder = product.MinimumOrder;
                prod.Price = product.Price;
                prod.CriticalLevel = product.CriticalLevel;

                db.Entry(prod).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                    
                return true;
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public bool DeleteProduct(int product_Id)
        {
            try
            {
                OptimumSource_ExamDbContext db = new OptimumSource_ExamDbContext();
                ProbOne_Products prod = new ProbOne_Products();
                prod = db.ProbOne_Products.Where(prd => prd.Id == product_Id).FirstOrDefault<ProbOne_Products>();

                using (var newContext = new OptimumSource_ExamDbContext())
                {
                    newContext.Entry(prod).State = System.Data.Entity.EntityState.Deleted;
                    newContext.SaveChanges();
                }  
                return true;
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public ProbOne_Products GetProductByProdCode(int id)
        {
            try
            {
                OptimumSource_ExamDbContext db = new OptimumSource_ExamDbContext();
                ProbOne_Products product = new ProbOne_Products();
                 
                var sample = db.ProbOne_Products.Where(prod => prod.Id == id).FirstOrDefault();
                return sample;
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public ProbOne_Products GetProductByProductId(int Product_Id)
        {
            try
            {
                OptimumSource_ExamDbContext db = new OptimumSource_ExamDbContext();
                ProbOne_Products product = new ProbOne_Products();

                var sample = db.ProbOne_Products.Where(prod => prod.Id == Product_Id).FirstOrDefault();
                return sample;
            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}
