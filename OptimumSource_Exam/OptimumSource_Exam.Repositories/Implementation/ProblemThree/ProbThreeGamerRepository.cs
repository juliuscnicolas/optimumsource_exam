﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OptimumSource_Exam.Models;
using OptimumSource_Exam.Repositories.UnitOfWork;

namespace OptimumSource_Exam.Repositories.Implementation.ProblemThree
{
    public class ProbThreeGamerRepository
    {
        public List<ProbThree_Gamers> GetAllGamer()
        {
            try
            {
                OptimumSource_ExamDbContext db = new OptimumSource_ExamDbContext();
                return db.ProbThree_Gamers.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool CreateGamer(ProbThree_Gamers gamer)
        {
            try
            {
                OptimumSource_ExamDbContext db = new OptimumSource_ExamDbContext();
                db.ProbThree_Gamers.Add(gamer);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool UpdateGamer(ProbThree_Gamers gamer)
        {
            try
            {
                return true;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool DeleteGamer(int gamerId)
        {
            try
            {
                OptimumSource_ExamDbContext db = new OptimumSource_ExamDbContext();
                ProbThree_Gamers game = new ProbThree_Gamers();
                game = db.ProbThree_Gamers.Where(gm => gm.Id == gamerId).FirstOrDefault<ProbThree_Gamers>();

                using (var newContext = new OptimumSource_ExamDbContext())
                {
                    newContext.Entry(game).State = System.Data.Entity.EntityState.Deleted;
                    newContext.SaveChanges();
                }  
                return true;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public ProbThree_Gamers GetGamerById(int gamerId)
        {
            try
            {
                OptimumSource_ExamDbContext db = new OptimumSource_ExamDbContext();
                var result = db.ProbThree_Gamers.Where(gmr => gmr.Id == gamerId).FirstOrDefault<ProbThree_Gamers>();
                return result;
            }
            catch (Exception)
            {
                
                throw;
            }

        }

    }
}
