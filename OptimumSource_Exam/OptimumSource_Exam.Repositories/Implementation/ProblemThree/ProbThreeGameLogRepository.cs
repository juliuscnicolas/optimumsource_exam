﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OptimumSource_Exam.Models;
using OptimumSource_Exam.Repositories.UnitOfWork;
using OptimumSource_Exam.Models.ViewModel.ProblemThree;

namespace OptimumSource_Exam.Repositories.Implementation.ProblemThree
{
    public class ProbThreeGameLogRepository
    {
        public List<GamerGameLogs> GetAllGameLog()
        {
            try
            {
                OptimumSource_ExamDbContext db = new OptimumSource_ExamDbContext();

                var result = from gameLog in db.ProbThree_GameLogs
                             join gamer in db.ProbThree_Gamers on gameLog.Gamer_Id equals gamer.Id
                             select new GamerGameLogs { Id = gamer.Id, GamerCode = gamer.GamerCode, Start = gameLog.Start, End = gameLog.End, EarnedPoints = gameLog.EarnedPoints, CurrentPoints = gameLog.CurrentPoints };

                return result.ToList<GamerGameLogs>();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool CreateGameLog(ProbThree_GameLogs gameLog)
        {
            try
            {
                OptimumSource_ExamDbContext db = new OptimumSource_ExamDbContext();
                db.ProbThree_GameLogs.Add(gameLog);
               

                var gamer = db.ProbThree_Gamers.Where(gmr => gmr.Id == gameLog.Gamer_Id).FirstOrDefault();
                gamer.Points = gameLog.CurrentPoints;
                db.Entry(gamer).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public bool UpdateGameLog(ProbThree_GameLogs gameLog)
        {
            try
            {
                return true;
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public bool DeleteGameLog(ProbThree_GameLogs gameLog)
        {
            try
            {
                return true;
            }
            catch (Exception)
            {
                
                throw;
            }
        }
    }
}
