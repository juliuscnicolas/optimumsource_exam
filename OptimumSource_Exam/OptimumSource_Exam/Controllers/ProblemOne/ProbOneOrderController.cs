﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OptimumSource_Exam.Models;
using Newtonsoft.Json;
using OptimumSource_Exam.Services.ProblemOne;
using OptimumSource_Exam.Models.ViewModel.ProblemOne;

namespace OptimumSource_Exam.Controllers.ProblemOne
{
    public class ProbOneOrderController : Controller
    {
        //
        // GET: /ProbOneOrder/

        public ActionResult Order()
        {
            return View("~/Views/ProblemOne/Order.cshtml");
        }

        public ActionResult AddOrder()
        {
            Session["IsOrderNew"] = true;
            List<ProbOne_Products> productList = new List<ProbOne_Products>();
            ProbOne_ProductServices productServices = new ProbOne_ProductServices();
            List<SelectListItem> productSelectList = new List<SelectListItem>();

            productList = productServices.GetAllProducts();

            foreach (var item in productList)
            {
                productSelectList.Add(new SelectListItem { Text = item.ProductCode, Value = item.Id.ToString() });
            }

            ViewBag.ProductCode = productSelectList;

            return View("~/Views/ProblemOne/AddOrder.cshtml");
        }

        [HttpPost]
        public ActionResult GetAllOrders(string sample)
        {
            try
            {
                List<ProductOrder> productOrderList = new List<ProductOrder>();
                ProbOne_OrderServices orderServices = new ProbOne_OrderServices();
                productOrderList = orderServices.GetAllOrders();

                var result = new ContentResult
                {
                    Content = JsonConvert.SerializeObject(productOrderList),
                    ContentType = "application/json",
                };

                return result;

            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPost]
        public ActionResult AddOrder(int ProductCode, int Quantity, decimal AmountDue, int QuantityBecomes, DateTime DatePurchased)
        {
            try
            {
                ProbOne_OrderServices orderServices = new ProbOne_OrderServices();
                ProbOne_Orders order = new ProbOne_Orders();
                order.Product_Id = ProductCode;
                order.DatePurchased = DatePurchased;
                order.Quantity = Quantity;
                order.AmountDue = AmountDue;
                order.CurrentProductQuantity = QuantityBecomes;
                orderServices.CreateOrder(order);
                return RedirectToAction("Order", "ProbOneOrder");
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        [HttpGet]
        public ActionResult CheckAmount(int quantity, string prodCode, DateTime datePurchased)
        {
            try
                {
                ProbOne_Orders orders = new ProbOne_Orders();
                orders.Quantity = quantity;
                orders.DatePurchased = datePurchased;
                orders.Product_Id = Convert.ToInt32(prodCode);
                
                    
                ProbOne_OrderServices orderServices = new ProbOne_OrderServices();
                var result = orderServices.CheckAmount(orders);
                return Json(result, JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {
                
                throw;
            }

        }

    }
}
