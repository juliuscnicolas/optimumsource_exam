﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using OptimumSource_Exam.Models;
using OptimumSource_Exam.Services.ProblemOne;

namespace OptimumSource_Exam.Controllers.ProblemOne
{
    public class ProbOneProductController : Controller
    {
        //
        // GET: /Product/
        public ActionResult Product()
        {
            return View("~/Views/ProblemOne/Product.cshtml");
        }

        [HttpPost]
        public ActionResult GetAllProducts(string sample)
        {
            try
            {
                List<ProbOne_Products> productList = new List<ProbOne_Products>();
                ProbOne_ProductServices productServices = new ProbOne_ProductServices();
                productList = productServices.GetAllProducts();
                
                var result = new ContentResult
                {
                    Content = JsonConvert.SerializeObject(productList),
                    ContentType = "application/json",
                };

                return result;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult AddProduct()
        {
            Session["IsProductNew"] = true;
            
            return View("~/Views/ProblemOne/AddProduct.cshtml");
        }

        [HttpGet]
        public ActionResult GenerateProductCode(string ProdDesc)
        { 
            DateTime dt = DateTime.Now;
            Random rand = new Random();
            var result = string.Format("{0}-{1}-{2}", ProdDesc.Substring(0, 3), string.Format("0{0}", rand.Next(1000,9000)), string.Format("{0:MMMddyyyy}", dt)).ToUpper(); 
            return Content(result, "text/plain");   
        }

        [HttpPost]
        public ActionResult AddProduct(string ProdCode, string Description, int Quantity, int BulkOrder, int MinOrder, decimal Price, int CriticalLevel)
        {
            try
            {
                ProbOne_ProductServices productServices = new ProbOne_ProductServices();
                ProbOne_Products product = new ProbOne_Products();
                product.ProductCode = ProdCode;
                product.ProductDescription = Description;
                product.Quantity = Quantity;
                product.BulkOrder = BulkOrder;
                product.MinimumOrder = MinOrder;
                product.Price = Price;
                product.CriticalLevel = CriticalLevel;
                productServices.CreateProduct(product);
                return RedirectToAction("Product", "ProbOneProduct");
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        [HttpGet]
        public ActionResult UpdateProduct(int ProductId)
        {
            try
            {
                ProbOne_Products prod = new ProbOne_Products();
                ProbOne_ProductServices productServices = new ProbOne_ProductServices();
                prod = productServices.GetProductByProductId(ProductId);

                Session["ProductId"] = ProductId;
                Session["ProductCode"] = prod.ProductCode;
                Session["ProductDesc"] = prod.ProductDescription;
                Session["Quantity"] = prod.Quantity;
                Session["BulkOrder"] = prod.BulkOrder;
                Session["MinimumOrder"] = prod.MinimumOrder;
                Session["Price"] = prod.Price;
                Session["CriticalLevel"] = prod.CriticalLevel;

                return View("~/Views/ProblemOne/UpdateProduct.cshtml");
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        [HttpPost]
        public ActionResult UpdateProduct(int ProdId, string ProdCode, string Description, int Quantity, int BulkOrder, int MinOrder, decimal Price, int CriticalLevel)
        {
            try
            {

                ProbOne_ProductServices productServices = new ProbOne_ProductServices();
                
                ProbOne_Products product = new ProbOne_Products();
                product.Id = ProdId;
                product.ProductCode = ProdCode;
                product.ProductDescription = Description;
                product.Quantity = Quantity;
                product.BulkOrder = BulkOrder;
                product.MinimumOrder = MinOrder;
                product.Price = Price;
                product.CriticalLevel = CriticalLevel;
                productServices.UpdateProduct(product);
                return View("~/Views/ProblemOne/Product.cshtml");
            }
            catch (Exception)
            {

                throw;
            }
        }
        
        [HttpPost]
        public ActionResult SaveProduct()
        {
            try
            {
                return View();
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public ActionResult DeleteProduct(int ProductId)
        {
            try
            {
                ProbOne_ProductServices prodServices = new ProbOne_ProductServices();
                var result = prodServices.DeleteProduct(ProductId);
                //return RedirectToAction("Product", "ProbOneProduct");
                return Content("Deleted", "text/plain");  
            }
            catch (Exception)
            {
                
                throw;
            }
            
        }

        //public ActionResult GetProductByProductId()
        //{ 
            
        //}

        


    }
}
