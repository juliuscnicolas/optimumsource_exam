﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using OptimumSource_Exam.Models;
using OptimumSource_Exam.Services.ProblemThree;
using System.Globalization;

namespace OptimumSource_Exam.Controllers.ProblemThree
{
    public class ProbThreeGamerController : Controller
    {
        //
        // GET: /ProblemThreeGamer/

        public ActionResult Gamer()
        {
            return View("~/Views/ProblemThree/Gamer.cshtml");
        }

        [HttpPost]
        public ActionResult GetAllGamers(string sample)
        {
            try
            {
                List<ProbThree_Gamers> gamerList = new List<ProbThree_Gamers>();
                ProbThree_GamerServices gamerServices = new ProbThree_GamerServices();
                gamerList = gamerServices.GetAllGamer();

                var result = new ContentResult
                {
                    Content = JsonConvert.SerializeObject(gamerList),
                    ContentType = "application/json",
                };

                return result;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult AddGamer()
        {
            Session["IsGamerNew"] = true;
            return View("~/Views/ProblemThree/AddGamer.cshtml");
        }

        [HttpGet]
        public ActionResult GenerateGamerCode(string Name, DateTime RegistrationDate, DateTime BirthDate)
        { 
            DateTime dt = DateTime.Now;
            Random rand = new Random();
            var result = string.Format("{0}-{1}-{2}-{3}", Name.Substring(0, 3), string.Format("{0:MMddyyyy}", RegistrationDate), string.Format("{0:MMddyyyy}", BirthDate), string.Format("0{0}", rand.Next(1000, 9000))).ToUpper(); 
            return Content(result, "text/plain");   
        }

        [HttpPost]
        public ActionResult AddGamer(string GamerCode, string Name, DateTime RegistrationDate, DateTime BirthDate)
        {
            try
            {
                ProbThree_GamerServices gamerServices = new ProbThree_GamerServices();
                ProbThree_Gamers gamer = new ProbThree_Gamers();

                gamer.GamerCode = GamerCode;
                gamer.Name = Name;
                gamer.RegistrationDate = Convert.ToDateTime(RegistrationDate);
                gamer.BirthDate = Convert.ToDateTime(BirthDate);
                gamer.Points = 500;
                gamerServices.CreateGamer(gamer);
                return RedirectToAction("Gamer", "ProbThreeGamer");
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult DeleteGamer(int GamerId)
        {
            try
            {
                ProbThree_GamerServices gamerServices = new ProbThree_GamerServices();
                var result = gamerServices.DeleteGamer(GamerId);
                //return RedirectToAction("Product", "ProbOneProduct");
                return Content("Deleted", "text/plain");
            }
            catch (Exception)
            {

                throw;
            }

        }

    }
}
