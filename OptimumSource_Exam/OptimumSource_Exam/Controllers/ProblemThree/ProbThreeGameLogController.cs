﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using OptimumSource_Exam.Models;
using OptimumSource_Exam.Services.ProblemThree;
using OptimumSource_Exam.Models.ViewModel.ProblemThree;


namespace OptimumSource_Exam.Controllers.ProblemThree
{
    public class ProbThreeGameLogController : Controller
    {
        //
        // GET: /ProbThreeGameLog/

        public ActionResult GameLog()
        {
            return View("~/Views/ProblemThree/GameLog.cshtml");
        }

        [HttpPost]
        public ActionResult GetAllGameLogs(string sample)
        {
            try
            {
                List<GamerGameLogs> gameLogList = new List<GamerGameLogs>();
                ProbThree_GameLogServices gameLogServices = new ProbThree_GameLogServices();
                gameLogList = gameLogServices.GetAllGameLog();

                var result = new ContentResult
                {
                    Content = JsonConvert.SerializeObject(gameLogList),
                    ContentType = "application/json",
                };

                return result;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult AddGameLog()
        {
            Session["IsGameLogNew"] = true;
            List<ProbThree_Gamers> gamerList = new List<ProbThree_Gamers>();
            ProbThree_GamerServices gamerServices = new ProbThree_GamerServices();
            List<SelectListItem> gamerSelectList = new List<SelectListItem>();

            gamerList = gamerServices.GetAllGamer();

            foreach (var item in gamerList)
            {
                gamerSelectList.Add(new SelectListItem { Text = item.GamerCode, Value = item.Id.ToString() });
            }

            ViewBag.GamerCode = gamerSelectList;

            return View("~/Views/ProblemThree/AddGameLog.cshtml");
        }

        [HttpPost]
        public ActionResult AddGameLog(int GamerCode, DateTime DateStarted, DateTime DateEnded, int EarnedPoints, int CurrentPoints)
        {
            try
            {
                ProbThree_GameLogServices gameLogServices = new ProbThree_GameLogServices();
                ProbThree_GameLogs gameLog = new ProbThree_GameLogs();
                gameLog.Gamer_Id = GamerCode;
                gameLog.Start = DateStarted;
                gameLog.End = DateEnded;
                gameLog.CurrentPoints = CurrentPoints;
                gameLog.EarnedPoints = EarnedPoints;
                gameLogServices.CreateGameLog(gameLog);
                return RedirectToAction("GameLog", "ProbThreeGameLog");
            }
            catch (Exception)
            {

                throw;
            }
        }


        [HttpGet]
        public ActionResult CheckPoints(DateTime dateStarted, DateTime dateEnded, int gamerId)
        {
            try
            {
                ProbThree_GameLogs gameLog = new ProbThree_GameLogs();
                gameLog.Start = dateStarted;
                gameLog.End = dateEnded;
                gameLog.Gamer_Id = gamerId;


                ProbThree_GameLogServices gameLogServices = new ProbThree_GameLogServices();
                var result = gameLogServices.CheckPoints(gameLog);
                return Json(result, JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {

                throw;
            }

        }


    }
}
