﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using OptimumSource_Exam.Models;
using OptimumSource_Exam.Services.ProblemTwo;

namespace OptimumSource_Exam.Controllers.ProblemTwo
{
    public class ProbTwoProductController : Controller
    {
        //
        // GET: /ProbTwoProduct/

        public ActionResult Product()
        {
            return View("~/Views/ProblemTwo/Product.cshtml");
        }

        public ActionResult AddProduct()
        {
            Session["IsProductNew"] = true;
            return View("~/Views/ProblemTwo/AddProduct.cshtml");
        }

        public ActionResult GetAllProducts(string sample)
        {
            try
            {
                List<ProbTwo_Products> productList = new List<ProbTwo_Products>();
                ProbTwo_ProductServices productServices = new ProbTwo_ProductServices();
                productList = productServices.GetAllProducts();

                var result = new ContentResult
                {
                    Content = JsonConvert.SerializeObject(productList),
                    ContentType = "application/json",
                };

                return result;

            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}
