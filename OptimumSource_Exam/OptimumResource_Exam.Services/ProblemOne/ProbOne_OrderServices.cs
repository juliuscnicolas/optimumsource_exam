﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OptimumSource_Exam.Models;
using OptimumSource_Exam.Repositories.Implementation;
using OptimumSource_Exam.Repositories.Implementation.ProblemOne;
using OptimumSource_Exam.Models.ViewModel.ProblemOne;

namespace OptimumSource_Exam.Services.ProblemOne
{
    public class ProbOne_OrderServices
    {
        public List<ProductOrder> GetAllOrders()
        {
            try
            {
                ProbOneOrderRepository orderRepository = new ProbOneOrderRepository();
                var result = orderRepository.GetAllOrders();
                return result;
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public bool CreateOrder(ProbOne_Orders order)
        {
            try
            {
                ProbOneOrderRepository orderRepository = new ProbOneOrderRepository();
                var result = orderRepository.CreateOrders(order);
                return result;
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public bool UpdateOrder(ProbOne_Orders order)
        {
            try
            {
                ProbOneOrderRepository orderRepository = new ProbOneOrderRepository();
                var result = orderRepository.UpdateOrder(order);
                return true;
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public bool DeleteOrder(ProbOne_Orders order)
        {
            try
            {
                ProbOneOrderRepository orderRepository = new ProbOneOrderRepository();
                var result = orderRepository.DeleteOrder(order);
                return true;
            }
            catch (Exception)
            {

                throw;
            }
               
        }

        public ProbOne_Orders CheckAmount(ProbOne_Orders order)
        {
            try
            {
                decimal surcharge = 0;
                decimal discount = 0;
                bool IsDiscounted = false;
                decimal amount = 0;
                int bulkAmount = 0;

                // get product
                // var product = 
                var product = new ProbOne_Products();
                ProbOneProductRepository productRepository = new ProbOneProductRepository();
                product = productRepository.GetProductByProdCode(order.Product_Id);


                if (order.Quantity < product.MinimumOrder)
                {
                    IsDiscounted = false;
                    surcharge = 14;
                    
                }
                if (order.Quantity >= product.MinimumOrder && order.Quantity < product.BulkOrder)
                {
                    IsDiscounted = true;
                    discount = 5;
                    
                }
                if (order.Quantity < product.CriticalLevel)
                {
                    bulkAmount = Convert.ToInt32(product.BulkOrder * Convert.ToDecimal(.400));
                    product.CriticalLevel = bulkAmount + product.CriticalLevel;
                }
                if (order.Quantity > product.BulkOrder)
                {
                    IsDiscounted = true;
                    discount = discount + 13;   
                }
                if ((int)order.DatePurchased.DayOfWeek == 1 || (int)order.DatePurchased.DayOfWeek == 3 || order.DatePurchased.Day == 5)
                {
                    IsDiscounted = true;
                    discount = discount + 9;
                }
                if ((int)order.DatePurchased.DayOfWeek == 6 || (int)order.DatePurchased.DayOfWeek == 7)
                {
                    IsDiscounted = true;
                    discount = discount + 11;
                }

                amount = (order.Quantity * product.Price);
                order.CurrentProductQuantity = product.Quantity - order.Quantity;
                if (IsDiscounted)
                {
                    order.AmountDue = amount - (amount * discount / 100);
                }
                else
                {
                    order.AmountDue = amount + (amount * surcharge / 100);
                }
                

                return order;

            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
