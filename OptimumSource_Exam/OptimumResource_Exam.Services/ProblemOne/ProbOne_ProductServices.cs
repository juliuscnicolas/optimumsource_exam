﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OptimumSource_Exam.Models;
using OptimumSource_Exam.Repositories.Implementation.ProblemOne;

namespace OptimumSource_Exam.Services.ProblemOne
{
    public class ProbOne_ProductServices
    {
        public List<ProbOne_Products> GetAllProducts()
        {
            try
            {
                ProbOneProductRepository problemRepository = new ProbOneProductRepository();
                return problemRepository.GetAllProducts();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool CreateProduct(ProbOne_Products product)
        {
            try
            {
                ProbOneProductRepository productRepository = new ProbOneProductRepository();
                var result = productRepository.CreateProduct(product);
                return result;
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public bool UpdateProduct(ProbOne_Products product)
        {
            try
            {
                ProbOneProductRepository productRepository = new ProbOneProductRepository();
                var result = productRepository.UpdateProduct(product);
                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool DeleteProduct(int product_Id)
        {
            try
            {
                ProbOneProductRepository productRepository = new ProbOneProductRepository();
                var result = productRepository.DeleteProduct(product_Id);
                return result;
            }
            catch (Exception)
            {

                throw;
            }

        }

        public ProbOne_Products GetProductByProductId(int Product_Id)
        {
            try
            {
                ProbOneProductRepository productRepository = new ProbOneProductRepository();
                var result = productRepository.GetProductByProductId(Product_Id);
                return result;
            }
            catch (Exception)
            {
                
                throw;
            }
        }
    }
}
