﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OptimumSource_Exam.Models;

namespace OptimumResource_Exam.Services
{
    public class OrderServices
    {
        public Boolean CreateOrder(ProbOne_Orders orders)
        {
            return true;
        }

        public ProbOne_Orders CheckAmount(ProbOne_Orders order)
        {
            try
            {
                decimal surcharge = 0;
                decimal discount = 0;
                decimal amount = 0;
                int bulkAmount = 0;

                // get product
                // var product = 
                var product = new ProbOne_Products();
                if (order.Quantity < product.Quantity)
                {
                    amount = (order.Quantity * product.Price);
                    surcharge = (amount * Convert.ToDecimal(.14));
                    order.AmountDue = amount + surcharge;
                }
                else if (order.Quantity >= product.MinimumOrder && order.Quantity < product.BulkOrder)
                {
                    amount = (order.Quantity * product.Price);
                    discount = (amount * Convert.ToDecimal(.05));
                    order.AmountDue = amount - discount;
                }
                else if (order.Quantity < product.CriticalLevel)
                {
                    bulkAmount = Convert.ToInt32(product.BulkOrder * Convert.ToDecimal(.400));
                    product.CriticalLevel = bulkAmount + product.CriticalLevel;
                }
                else if (order.Quantity > product.BulkOrder)
                {
                    amount = order.Quantity * product.Price;
                    discount = amount * Convert.ToDecimal(.13);
                    order.AmountDue = amount - discount;
                }
                else if (order.DatePurchased.Day == 1 || order.DatePurchased.Day == 3 || order.DatePurchased.Day == 5)
                {
                    amount = order.Quantity * product.Price;
                    discount = amount * Convert.ToDecimal(.09);
                    order.AmountDue = amount - discount;
                }
                else if (order.DatePurchased.Day == 6 || order.DatePurchased.Day == 7)
                {
                    amount = order.Quantity * product.Price;
                    discount = amount * Convert.ToDecimal(.11);
                    order.AmountDue = amount - discount;
                }

                return order;

            }
            catch (Exception)
            {
                throw;
            }
        }


    }
}
