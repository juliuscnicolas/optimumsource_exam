﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OptimumSource_Exam.Models;
using OptimumSource_Exam.Repositories.Implementation.ProblemThree;

namespace OptimumSource_Exam.Services.ProblemThree
{
    public class ProbThree_GamerServices
    {
        public List<ProbThree_Gamers> GetAllGamer()
        {
            try
            {
                ProbThreeGamerRepository gamerRepository = new ProbThreeGamerRepository();
                var result = gamerRepository.GetAllGamer();
                return result;
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public bool CreateGamer(ProbThree_Gamers gamer)
        {
            try
            {
                ProbThreeGamerRepository gamerRepository = new ProbThreeGamerRepository();
                var result = gamerRepository.CreateGamer(gamer);
                return result;
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public bool UpdateGamer(ProbThree_Gamers gamer)
        {
            try
            {
                ProbThreeGamerRepository gamerRepository = new ProbThreeGamerRepository();
                var result = gamerRepository.UpdateGamer(gamer);
                return result;
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public bool DeleteGamer(int gamerId)
        {
            try
            {
                ProbThreeGamerRepository gamerRepository = new ProbThreeGamerRepository();
                var result = gamerRepository.DeleteGamer(gamerId);
                return result;
            }
            catch (Exception)
            {

                throw;
            }
               
        }


    }
}
