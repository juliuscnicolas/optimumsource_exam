﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OptimumSource_Exam.Models;
using OptimumSource_Exam.Repositories.Implementation.ProblemThree;
using OptimumSource_Exam.Models.ViewModel.ProblemThree;

namespace OptimumSource_Exam.Services.ProblemThree
{
    public class ProbThree_GameLogServices
    {
        public List<GamerGameLogs> GetAllGameLog()
        {
            try
            {
                ProbThreeGameLogRepository gameLogRepository = new ProbThreeGameLogRepository();
                return gameLogRepository.GetAllGameLog();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool CreateGameLog(ProbThree_GameLogs gameLog)
        {
            try
            {
                ProbThreeGameLogRepository gameLogRepository = new ProbThreeGameLogRepository();
                var result = gameLogRepository.CreateGameLog(gameLog);
                return result;
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public bool UpdateGameLog(ProbThree_GameLogs gameLog)
        {
            try
            {
                ProbThreeGameLogRepository gameLogRepository = new ProbThreeGameLogRepository();
                var result = gameLogRepository.UpdateGameLog(gameLog);
                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool DeleteGameLog(ProbThree_GameLogs gameLog)
        {
            try
            {
                ProbThreeGameLogRepository gameLogRepository = new ProbThreeGameLogRepository();
                var result = gameLogRepository.DeleteGameLog(gameLog);
                return result;
            }
            catch (Exception)
            {

                throw;
            }

        }
        public ProbThree_GameLogs CheckPoints(ProbThree_GameLogs gameLog)
        {
            try
            {
                // get gamer information
                ProbThreeGamerRepository gamerRepository = new ProbThreeGamerRepository();
                var gamer = gamerRepository.GetGamerById(gameLog.Gamer_Id);
                ProbThree_GameLogs gameLogResult = new ProbThree_GameLogs();
                //var age = DateTime.Now - gamer.BirthDate;

                int age = (Int32.Parse(DateTime.Today.ToString("yyyyMMdd")) - Int32.Parse(gamer.BirthDate.ToString("yyyyMMdd"))) / 10000;
                int DayPlayed = Convert.ToInt16(gameLog.Start.DayOfWeek);
                int points = 0;
                int minutesPlayed = Convert.ToInt32(gameLog.End.Subtract(gameLog.Start).TotalMinutes);
                bool IsAdult = age > 17;

                switch (DayPlayed)
                {
                    case 0:
                        points =  Convert.ToInt32(minutesPlayed / 2) * 20;
                        break;
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                        if (IsAdult)
                        {
                            points = Convert.ToInt32(minutesPlayed / 5) * 5;    
                        }
                        break;
                    case 5:
                        if (IsAdult)
                        {
                            points = Convert.ToInt32(minutesPlayed / 5) * 10;
                        }
                        break;
                    case 6:
                        points = Convert.ToInt32(minutesPlayed / 3) * 25;
                        break;
                    
                }
                   
                gameLogResult.EarnedPoints = points;
                gameLogResult.CurrentPoints = gamer.Points + points;

                return gameLogResult;

            }
            catch (Exception)
            {
                throw;
            }
        }
 
    }
}
