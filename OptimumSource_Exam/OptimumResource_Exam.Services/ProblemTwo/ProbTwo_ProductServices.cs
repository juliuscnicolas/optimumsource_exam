﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OptimumSource_Exam.Models;
using OptimumSource_Exam.Repositories.Implementation.ProblemTwo;

namespace OptimumSource_Exam.Services.ProblemTwo
{
    public class ProbTwo_ProductServices
    {
        public List<ProbTwo_Products> GetAllProducts()
        {
            try
            {
                ProbTwoProductRepository problemRepository = new ProbTwoProductRepository();
                return problemRepository.GetAllProducts();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool CreateProduct(ProbTwo_Products product)
        {
            try
            {
                ProbTwoProductRepository productRepository = new ProbTwoProductRepository();
                var result = productRepository.CreateProduct(product);
                return result;
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public bool UpdateProduct(ProbTwo_Products product)
        {
            try
            {
                return true;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool DeleteProduct(ProbTwo_Products product)
        {
            try
            {
                ProbTwoProductRepository productRepository = new ProbTwoProductRepository();
                var result = productRepository.DeleteProduct(product);
                return result;
            }
            catch (Exception)
            {

                throw;
            }

        }

        
    }
}
