﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace OptimumSource_Exam.Models
{
    public class ProbThree_Gamers
    {
        [Key]
        public int Id { get; set; }
        public string GamerCode { get; set; }
        public string Name { get; set; }
        public DateTime RegistrationDate {get; set;}
        public DateTime BirthDate {get; set;}
        public int Points {get; set;}
    }
}
