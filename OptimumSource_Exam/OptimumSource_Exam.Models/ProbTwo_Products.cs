﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;


namespace OptimumSource_Exam.Models
{
    public class ProbTwo_Products
    {
        [Key]
        public int Id { get; set; }
        public string ProductCode { get; set; }
        public string ProductDescription { get; set; }
        public decimal Price { get; set; }
        public bool IsDiscounted { get; set; }
        public int Discount { get; set; }
        public DateTime DateAdded { get; set; }
        public int CriticalLevel { get; set; }
    }
}
