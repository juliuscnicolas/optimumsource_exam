﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace OptimumSource_Exam.Models
{
    public class ProbOne_Orders
    {
        [Key]
        public int Id { get; set; }

        //[ForeignKey("Id")]
        //public ProbOne_Products ProbOne_Products { get; set; }
        //[ForeignKey("ProbOne_Products")]
        public int Product_Id { get; set; }
        public int Quantity { get; set; }
        public decimal AmountDue { get; set; }
        public int CurrentProductQuantity { get; set; }
        public DateTime DatePurchased { get; set; }
    }
}
