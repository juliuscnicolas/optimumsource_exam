﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptimumSource_Exam.Models.ViewModel.ProblemThree
{
    public class GamerGameLogs
    {
        public int Id { get; set; }
        public string GamerCode { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public int EarnedPoints { get; set; }
        public int CurrentPoints { get; set; }

    }
}
