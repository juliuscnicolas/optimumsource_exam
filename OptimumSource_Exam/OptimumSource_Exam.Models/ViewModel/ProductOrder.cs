﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptimumSource_Exam.Models.ViewModel.ProblemOne
{
    public class ProductOrder
    {
        public int OrderId { get; set; }
        public string ProductCode { get; set; }
        public int Quantity { get; set; }
        public decimal AmountDue { get; set; }
        public DateTime DatePurchased { get; set; }
        public string QuantityBecomes { get; set; }
    }
    
}
