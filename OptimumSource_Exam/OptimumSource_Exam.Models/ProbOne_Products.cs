﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;



namespace OptimumSource_Exam.Models
{
    public class ProbOne_Products
    {
        [Key]
        public int Id { get; set; }
        public string ProductCode { get; set; }
        public string ProductDescription { get; set; }
      
        public int Quantity { get; set; }
        public int BulkOrder { get; set; }
        public int MinimumOrder { get; set; }
        public decimal Price { get; set; }
        public int CriticalLevel { get; set; }
    }
}
