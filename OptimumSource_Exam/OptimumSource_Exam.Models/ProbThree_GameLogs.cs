﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace OptimumSource_Exam.Models
{
    public class ProbThree_GameLogs
    {
        [Key]
        public int Id { get; set; }
        public int Gamer_Id { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public int EarnedPoints { get; set; }
        public int CurrentPoints { get; set; }

    }
}
